import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Card } from '../../models/card/card.model';
import { CardsService } from '../../services/cards.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {

  cards:Card[];
  totalTasks: number;

  @Output() totalTasksEvent = new EventEmitter<number>();

  constructor(private _cardsService:CardsService) {
    this.cards = [];
  }


  ngOnInit(): void {
    let self = this;
    self._cardsService
        .getCards()
        .subscribe(response => this.cards = response);

    this._cardsService.currTotalTasks.subscribe(totalTasks => this.totalTasks = totalTasks);
  }

  addCard(card:Card) {
    this._cardsService.addCard(card);
  }

  removeCard(index:number) {
    this._cardsService.removeCard(index);
    console.log(index);
  }

}
