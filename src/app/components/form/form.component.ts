import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { Card } from '../../models/card/card.model';
import { CardsService } from '../../services/cards.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  card:Card = {
    titulo:'',
    responsable:'',
    descripcion:'',
    prioridad:'baja'
  }

  @Output() cardAdded = new EventEmitter<Card>();

  public cardForm: FormGroup; // our model driven form
  public submitted: boolean; // keep track on whether form is submitted
  public events: any[] = []; // use later to display form changes

  constructor(private _fb: FormBuilder, private _cardsService:CardsService) { } // form builder simplify form initialization

  ngOnInit() {
    this.cardForm = this._fb.group({
        titulo: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
        responsable: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
        descripcion: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
        prioridad: ['baja']
    })
  }

  saveCard(model: Card, isValid: boolean) {
    this.submitted = true; // set form submit to true

    // check if model is valid
    // if valid, call API to save customer
    if(!isValid){
      console.log('Form not valid')
    }else{
      this._cardsService.addCard(model);
      this.cardAdded.emit(model);
      this.cardForm.reset();
      console.log(model, isValid);
    }
  }

}
