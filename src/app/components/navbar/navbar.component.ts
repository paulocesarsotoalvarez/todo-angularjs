import { Component, OnInit } from '@angular/core';

import { CardsService } from '../../services/cards.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  title:string;
  totalTasks: number;

  constructor(private _cardsService:CardsService) { }

  ngOnInit() {
    this.title='Tasks';
    this._cardsService.currTotalTasks.subscribe(totalTasks => this.totalTasks = totalTasks);
  }

}
