import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

import { Card } from '../models/card/card.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  private url="./assets/todos.json";

  private todos:any = [];

  // Tomado de:  https://www.youtube.com/watch?v=I317BhehZKM
  private totalTasksSource = new BehaviorSubject<number>(0);
  currTotalTasks = this.totalTasksSource.asObservable();

  constructor(private http:HttpClient) { }

  getCards(): Observable < Card[] >{
    return this
           .http.get(`${this.url}`)
           .pipe(map((response: any) => {
             this.updateTotalTasks(response.todos.length);
             this.todos = response;
             console.log(this.todos);
             return <Card[]> response
            }));
  }

  addCard(card:Card) {
    this.todos.todos.unshift(card);
    this.updateTotalTasks(this.todos.todos.length);
  }

  removeCard(index) {
    let todos = this.todos.todos.filter(function(e, i){
      return i !== index;
    });
    this.todos.todos = todos;
    this.updateTotalTasks(this.todos.todos.length);
  }
  updateTotalTasks(tasks: number) {
    console.log(tasks);
    this.totalTasksSource.next(tasks);
  }

}
