import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'ToDo AngularJs';
  totalTasks = 0;

  // Tomado de:  https://www.youtube.com/watch?v=I317BhehZKM
  receiveTotalTasks($event) {
    this.totalTasks = $event;
  }
}
